import React from 'react';
import ReactDOM from 'react-dom';
import './main.css';
import App from './components/_App/App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render( < App / > , document.getElementById('root'));
registerServiceWorker();