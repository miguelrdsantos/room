import React, { Component } from 'react';
import NotFound from './NotFound';
import Home from '../_Pages/Home';
import Project from '../_Pages/Project';
import Create from '../_Create/Create';
import Header from '../Header';
import Footer from '../Footer';
import './App.css';
import { Switch, Route, Router } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
// import Json from './Json';

const history = createBrowserHistory();
history.scrollRestoration = 'manual';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isScrolled: false,
			isSkillDesign: false,
			isSkillFrontend: false,
			isSkillUX: false,
			isFromProject: false,
			isFromHome: false,
			scrollHome: 0
		};
	}

	componentWillMount() {
		history.listen(() => {
			if (history.location.pathname.split('/')[1] === 'project') {
				window.scrollTo(0, 0);
			} else {
				window.scrollTo(0, this.state.scrollHome);
			}
			// TODO no scroll home/home

			this.lastPath = localStorage.getItem('lastPath');
			switch (this.lastPath.split('/')[1]) {
				case '':
					this.setState({
						isFromHome: true,
						isFromProject: false
					});
					break;
				case 'project':
					this.setState({
						isFromHome: false,
						isFromProject: true
					});
					break;
				default:
					return;
			}
		});
	}

	componentDidMount() {
		window.addEventListener(
			'scroll',
			() => {
				if (history.location.pathname.split('/')[1] === '') {
					this.setState({ scrollHome: window.pageYOffset });
				}
				let scrollTop =
					window.pageYOffset ||
					(document.documentElement || document.body.parentNode || document.body).scrollTop;
				this.setState({ isScrolled: scrollTop > 40 });
			},
			false
		);
	}

	render() {
		let elemClass = [ 'App' ];
		if (this.state.isScrolled) {
			elemClass.push('is-scrolled');
		}
		if (this.state.isSkillDesign) {
			elemClass.push('is-skill-design');
		}
		if (this.state.isSkillFrontend) {
			elemClass.push('is-skill-frontend');
		}
		if (this.state.isSkillUX) {
			elemClass.push('is-skill-ux');
		}
		if (this.state.isFromProject) {
			elemClass.push('is-from_project');
		}
		if (this.state.isFromHome) {
			elemClass.push('is-from_home');
		}

		// console.log(history);

		return (
			<div className={elemClass.join(' ')}>
				<Router history={history}>
					<div>
						<Header />
						<main className="main">
							<Switch>
								<Route exact path="/" component={Home} />
								<Route path="/project/:id" component={Project} />
								<Route path="/create" component={Create} />
								<Route component={NotFound} />
							</Switch>
						</main>
						<Footer />
					</div>
				</Router>
			</div>
		);
	}
}

export default App;
