import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Icon from '../Icon';

class NotFound extends Component {
	render() {
		return (
			<main>
				<section className="container">
					{' '}
					<Link className="link link--back" to="/">
						<Icon name="back" fill="#F2f2f2" title="Back icon" />
						Go home
					</Link>
					<h1 className="title-h1 bold">Page not found</h1>
					<span className="title-caption" />
				</section>
			</main>
		);
	}
}

export default NotFound;
