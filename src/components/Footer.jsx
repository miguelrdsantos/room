import React, { Component } from 'react';

class Header extends Component {
	render() {
		return (
			<footer className="footer">
				<div className="container footer-wrapper">
					<span className="footer-text">© 2018 Miguel Santos’ Room</span>
				</div>
			</footer>
		);
	}
}

export default Header;
