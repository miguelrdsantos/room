import React, { Component } from 'react';
import DelayLink from './DelayLink';
import TempImage1 from '../assets/images/temp/1.jpg';
import HB from '../assets/images/temp/headbanging_bird.svg';
import TempImage2 from '../assets/images/temp/2.jpg';
import TempImage3 from '../assets/images/temp/3.jpg';
import TempImage4 from '../assets/images/temp/4.jpg';
import TempImage5 from '../assets/images/temp/5.jpg';

class Thumbnail extends Component {
	tempImages = [ TempImage1, TempImage2, TempImage3, TempImage4, TempImage5 ];
	render() {
		var sectionStyle = {
			//backgroundImage: `url(${this.tempImages[this.props.id - 1]})`
			backgroundImage: `url(${HB})`
		};
		return (
			<article className={'thumbnail width-' + this.props.size + '/12'}>
				<DelayLink
					to={'/project/' + this.props.id}
					className="thumbnail__link"
					delay={300}
					onDelayStart={() => {
						document.getElementById('root').className = 'leave-to_project';
					}}
					onDelayEnd={() => {
						document.getElementById('root').className = '';
					}}
				>
					<div style={sectionStyle} className="thumbnail__image" />
					<h3 className="thumbnail__title">Project {this.props.id}</h3>
					<span className="thumbnail__description">Lorem ipsum dolor sit amet</span>
				</DelayLink>
			</article>
		);
	}
}

export default Thumbnail;
