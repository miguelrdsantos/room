import React, { Component } from 'react';
import DelayLink from '../DelayLink';
import Icon from '../Icon';
import TempImage1 from '../../assets/images/temp/1.jpg';
import TempImage2 from '../../assets/images/temp/2.jpg';
import TempImage3 from '../../assets/images/temp/3.jpg';
import TempImage4 from '../../assets/images/temp/4.jpg';
import TempImage5 from '../../assets/images/temp/5.jpg';

class Project extends Component {
	tempImages = [ TempImage1, TempImage2, TempImage3, TempImage4, TempImage5 ];

	componentWillMount() {
		localStorage.setItem('lastPath', this.props.location.pathname);
	}

	render() {
		return (
			<main className="project-page" id="project-page">
				<section className="container">
					{' '}
					<DelayLink
						to="/"
						className="link link--back"
						delay={300}
						onDelayStart={() => {
							document.getElementById('root').className = 'leave-to_home';
						}}
						onDelayEnd={() => {
							document.getElementById('root').className = '';
						}}
					>
						<Icon name="back" fill="#F2f2f2" title="Back icon" />
						Home
					</DelayLink>
					<h1 className="title-h1 bold">Project {this.props.match.params.id}</h1>
					<span className="title-caption step-4">
						Lorem ipsum dolor{' '}
						<a href="" className="link">
							a link
						</a>{' '}
						sit amet.
					</span>
					<div className="publish-container">
						<p>
							<span className="drop_cap">L</span>orem ipsum dolor sit amet, consectetur adipiscing elit.
							In a elementum metus. Nunc non dapibus felis. <a href="">Praesent id enim</a> tempor,
							sollicitudin metus nec, rhoncus nisl. Pellentesque placerat sagittis quam sit amet luctus.
							Sed velit libero, bibendum eget porttitor ut, pharetra at tellus. Aenean sapien ex,
							pellentesque vitae magna vel, viverra tincidunt justo. Cras sed quam magna. Nam id lacus eu
							dui sollicitudin volutpat.
						</p>
						<img src={this.tempImages[this.props.match.params.id - 1]} alt="Exemple" />
						<h3>Morbi sit amet congue</h3>
						<p>
							Aenean congue, neque eu sodales imperdiet, sapien est commodo ante, porta auctor mauris diam
							nec ex. Nulla urna diam, consequat in auctor id, congue quis justo. Fusce feugiat semper
							commodo. Morbi sit amet congue sem. Proin dictum nisl at elit hendrerit, non porta tortor
							pulvinar. Etiam efficitur augue quis leo sagittis, eu ornare arcu rutrum. Aliquam
							ullamcorper pellentesque nulla, luctus mattis urna sollicitudin vitae. Cras fermentum nulla
							aliquet arcu blandit posuere. Integer convallis erat ut tortor lacinia dignissim. Proin
							metus arcu, fermentum vel velit vitae, tempus pharetra augue. In euismod interdum volutpat.
							Nullam vitae varius risus. Morbi non sollicitudin libero.
						</p>
					</div>
				</section>
			</main>
		);
	}
}

export default Project;
