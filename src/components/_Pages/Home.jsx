import React, { Component } from 'react';
import IllustrationLogo from '../IllustrationLogo';
import Thumbnail from '../Thumbnail';

class Home extends Component {
	componentWillMount() {
		localStorage.setItem('lastPath', this.props.location.pathname);
	}

	render() {
		let elemClass = [ 'home-page' ];
		return (
			<main className={elemClass.join(' ')} id="home-page">
				<section className="container">
					<div className="home-wrapper">
						<div className="home-wrapper__illustration">
							<IllustrationLogo />
						</div>
						<div className="home-wrapper__text">
							<div className="home-wrapper__text-content">
								<h1 className="title-h1">
									<span className="step-1">Hello,</span>
									<br />
									<span className="step-2">
										i’m <span className="bold">Miguel</span>
										<br />
										<span className="bold step-3">Santos</span>
									</span>
								</h1>
								<span className="title-caption step-4">I do graphic design, frontend and UX.</span>
								<br />
								<a className="button" href="mailto:miguel@room.com">
									Contact-me
								</a>
							</div>
						</div>
					</div>
				</section>
				<section className="section--featured">
					<div className="container">
						<div className="thumbnail-wrapper thumbnail-wrapper--featured grid">
							<Thumbnail size="4" id="1" />
							<Thumbnail size="4" id="2" />
							<Thumbnail size="4" id="2" />
							<Thumbnail size="4" id="3" />
							<Thumbnail size="4" id="4" />
							<Thumbnail size="4" id="4" />
						</div>
					</div>
				</section>
				<section>
					<div className="container">
						<div className="thumbnail-wrapper grid">
							<Thumbnail size="3" id="1" />
							<Thumbnail size="3" id="2" />
							<Thumbnail size="3" id="3" />
							<Thumbnail size="3" id="4" />
							<Thumbnail size="3" id="5" />
							<Thumbnail size="3" id="6" />
							<Thumbnail size="3" id="7" />
							<Thumbnail size="3" id="8" />
						</div>
					</div>
				</section>
			</main>
		);
	}
}

export default Home;
