import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Logo extends Component {
	constructor(props) {
		super(props);
		this.state = { isActive: false, isAnimating: false };
	}
	logoElem;

	componentDidMount() {
		this.logoElem = document.getElementById('logo');
		this.logoElem.addEventListener(
			'mouseenter',
			() => {
				this.setState({ isActive: false });
				let self = this;
				setTimeout(function() {
					self.setState({ isAnimating: true });
				}, 500);
			},
			false
		);

		this.logoElem.addEventListener(
			'mouseleave',
			() => {
				this.setState({ isActive: true });
				this.setState({ isAnimating: false });
			},
			false
		);
	}

	render() {
		let elemClass = [ 'logo' ];
		if (this.state.isActive) {
			elemClass.push('is-active');
		}
		if (this.state.isAnimating) {
			elemClass.push('is-temp');
		}
		return (
			<Link to="/">
				<div id="logo" className={elemClass.join(' ')}>
					<span className="logo--r">R</span>
					<span className="logo--o1">o</span>
					<span className="logo--o2">o</span>
					<span className="logo--m">m</span>
				</div>
			</Link>
		);
	}
}

export default Logo;
