import React, { Component } from 'react';

class Icon extends Component {
	name = this.props.name || 'back';
	title = this.props.title || 'Icon';

	render() {
		switch (this.name) {
			case 'back':
				return (
					<svg
						className="icon"
						version="1.1"
						id="icon--back"
						xmlns="http://www.w3.org/2000/svg"
						xlink="http://www.w3.org/1999/xlink"
						x="0px"
						y="0px"
						viewBox="0 0 96.6 51.8"
						space="preserve"
						aria-labelledby="title"
					>
						<title id="title">{this.title}</title>
						<polygon points="32.3,45.5 17.3,30.4 96.6,30.4 96.6,21.4 17.3,21.4 32.3,6.3 26,0 0,25.9 26,51.8 	" />
					</svg>
				);
			case 'email':
				return (
					<svg
						className="icon"
						version="1.1"
						id="icon--back"
						xmlns="http://www.w3.org/2000/svg"
						xlink="http://www.w3.org/1999/xlink"
						x="0px"
						y="0px"
						viewBox="0 0 96.6 51.8"
						space="preserve"
						aria-labelledby="title"
					>
						<title id="title">{this.title}</title>
						<polygon points="32.3,45.5 17.3,30.4 96.6,30.4 96.6,21.4 17.3,21.4 32.3,6.3 26,0 0,25.9 26,51.8 	" />
					</svg>
				);
			default:
				return false;
		}
	}
}

export default Icon;
