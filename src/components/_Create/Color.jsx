import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { BlockPicker } from 'react-color';
import colorIcon from '../../assets/images/wysiwyg/color.svg';

class ColorPic extends Component {
	static propTypes = {
		expanded: PropTypes.bool,
		onExpandEvent: PropTypes.func,
		onChange: PropTypes.func,
		currentState: PropTypes.object
	};

	stopPropagation = (event) => {
		event.stopPropagation();
	};

	onChange = (color) => {
		const { onChange } = this.props;
		onChange('color', color.hex);
	};

	renderModal = () => {
		this.props.currentState.color = '#222222';
		const { color } = this.props.currentState;
		return (
			<div className="rdw-option--color" onClick={this.stopPropagation}>
				<BlockPicker
					colors={[ '#222222', '#2c3f5b', 'white', '#f2f2f2', '#F8B550', '#E87777' ]}
					color={color}
					onChangeComplete={this.onChange}
				/>
			</div>
		);
	};

	render() {
		const { expanded, onExpandEvent } = this.props;
		return (
			<div
				className="rdw-text-align-wrapper"
				aria-haspopup="true"
				aria-expanded={expanded}
				aria-label="rdw-color-picker"
			>
				<div className="rdw-option-wrapper" onClick={onExpandEvent}>
					<img src={colorIcon} alt="" />
				</div>
				{expanded ? this.renderModal() : undefined}
			</div>
		);
	}
}

export default ColorPic;
