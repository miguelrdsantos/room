import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Icon from '../Icon';
import Wysiwyg from './Wysiwyg';

class Create extends Component {
	render() {
		return (
			<main>
				<section className="container">
					{' '}
					<Link className="link link--back" to="/">
						<Icon name="back" fill="#F2f2f2" title="Back icon" />
						Home
					</Link>
					<h1 className="title-h1 bold">Create</h1>
					<span className="title-caption step-4">
						Lorem ipsum dolor{' '}
						<a href="" className="link">
							a link
						</a>{' '}
						sit amet.
					</span>
					<Wysiwyg />
				</section>
			</main>
		);
	}
}

export default Create;
