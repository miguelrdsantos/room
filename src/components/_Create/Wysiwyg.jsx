import React, { Component } from 'react';
import { convertFromRaw, convertToRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToMarkdown from 'draftjs-to-markdown';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './Wysiwyg.css';
import ColorPic from './Color';
import bold from '../../assets/images/wysiwyg/bold.svg';
import italic from '../../assets/images/wysiwyg/italic.svg';
import underline from '../../assets/images/wysiwyg/underline.svg';
import strikethrough from '../../assets/images/wysiwyg/strikethrough.svg';
import monospace from '../../assets/images/wysiwyg/code.svg';
import superscript from '../../assets/images/wysiwyg/superscript.svg';
import subscript from '../../assets/images/wysiwyg/subscript.svg';
import fontSize from '../../assets/images/wysiwyg/fontSize.svg';
import emoji from '../../assets/images/wysiwyg/emoji.svg';
import embedded from '../../assets/images/wysiwyg/embedded.svg';
import image from '../../assets/images/wysiwyg/image.svg';
import remove from '../../assets/images/wysiwyg/remove.svg';
import unordered from '../../assets/images/wysiwyg/unordered.svg';
import ordered from '../../assets/images/wysiwyg/ordered.svg';
import indent from '../../assets/images/wysiwyg/indent.svg';
import outdent from '../../assets/images/wysiwyg/outdent.svg';
import left from '../../assets/images/wysiwyg/left.svg';
import center from '../../assets/images/wysiwyg/center.svg';
import right from '../../assets/images/wysiwyg/right.svg';
import justify from '../../assets/images/wysiwyg/justify.svg';
import link from '../../assets/images/wysiwyg/link.svg';
import unlink from '../../assets/images/wysiwyg/unlink.svg';
import undo from '../../assets/images/wysiwyg/undo.svg';
import redo from '../../assets/images/wysiwyg/redo.svg';

const content = {
	entityMap: {},
	blocks: [
		{
			key: '637gr',
			text: 'Initialized from content state.',
			type: 'unstyled',
			depth: 0,
			inlineStyleRanges: [],
			entityRanges: [],
			data: {}
		}
	],
	toolbarOptions: {
		options: [
			'inline',
			'blockType',
			'fontSize',
			'fontFamily',
			'list',
			'textAlign',
			'colorPicker',
			'link',
			'embedded',
			'emoji',
			'image',
			'remove',
			'history'
		],
		inline: {
			inDropdown: false,
			className: undefined,
			component: undefined,
			dropdownClassName: undefined,
			options: [ 'bold', 'italic', 'underline', 'strikethrough', 'monospace', 'superscript', 'subscript' ],
			bold: { icon: bold, className: undefined },
			italic: { icon: italic, className: undefined },
			underline: { icon: underline, className: undefined },
			strikethrough: { icon: strikethrough, className: undefined },
			monospace: { icon: monospace, className: undefined },
			superscript: { icon: superscript, className: undefined },
			subscript: { icon: subscript, className: undefined }
		},
		blockType: {
			inDropdown: false,
			options: [ 'Normal', 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'Blockquote', 'Code' ],
			className: undefined,
			component: undefined,
			dropdownClassName: undefined
		},
		fontSize: {
			icon: fontSize,
			options: [ 8, 9, 10, 11, 12, 14, 16, 18, 24, 30, 36, 48, 60, 72, 96 ],
			className: undefined,
			component: undefined,
			dropdownClassName: undefined
		},
		fontFamily: {
			options: [ 'Lora', 'Cambria' ],
			// options: [ 'Arial', 'Georgia', 'Impact', 'Tahoma', 'Times New Roman', 'Verdana' ],
			className: undefined,
			component: undefined,
			dropdownClassName: undefined
		},
		list: {
			inDropdown: false,
			className: undefined,
			component: undefined,
			dropdownClassName: undefined,
			options: [ 'unordered', 'ordered', 'indent', 'outdent' ],
			unordered: { icon: unordered, className: undefined },
			ordered: { icon: ordered, className: undefined },
			indent: { icon: indent, className: undefined },
			outdent: { icon: outdent, className: undefined }
		},
		textAlign: {
			inDropdown: false,
			className: undefined,
			component: undefined,
			dropdownClassName: undefined,
			options: [ 'left', 'center', 'right', 'justify' ],
			left: { icon: left, className: undefined },
			center: { icon: center, className: undefined },
			right: { icon: right, className: undefined },
			justify: { icon: justify, className: undefined }
		},
		colorPicker: { component: ColorPic },
		link: {
			inDropdown: false,
			className: undefined,
			component: undefined,
			popupClassName: undefined,
			dropdownClassName: undefined,
			showOpenOptionOnHover: true,
			defaultTargetOption: '_self',
			options: [ 'link', 'unlink' ],
			link: { icon: link, className: undefined },
			unlink: { icon: unlink, className: undefined }
		},
		emoji: {
			icon: emoji,
			className: undefined,
			component: undefined,
			popupClassName: undefined,
			emojis: [
				'😀',
				'😁',
				'😂',
				'😃',
				'😉',
				'😋',
				'😎',
				'😍',
				'😗',
				'🤗',
				'🤔',
				'😣',
				'😫',
				'😴',
				'😌',
				'🤓',
				'😛',
				'😜',
				'😠',
				'😇',
				'😷',
				'😈',
				'👻',
				'😺',
				'😸',
				'😹',
				'😻',
				'😼',
				'😽',
				'🙀',
				'🙈',
				'🙉',
				'🙊',
				'👼',
				'👮',
				'🕵',
				'💂',
				'👳',
				'🎅',
				'👸',
				'👰',
				'👲',
				'🙍',
				'🙇',
				'🚶',
				'🏃',
				'💃',
				'⛷',
				'🏂',
				'🏌',
				'🏄',
				'🚣',
				'🏊',
				'⛹',
				'🏋',
				'🚴',
				'👫',
				'💪',
				'👈',
				'👉',
				'👉',
				'👆',
				'🖕',
				'👇',
				'🖖',
				'🤘',
				'🖐',
				'👌',
				'👍',
				'👎',
				'✊',
				'👊',
				'👏',
				'🙌',
				'🙏',
				'🐵',
				'🐶',
				'🐇',
				'🐥',
				'🐸',
				'🐌',
				'🐛',
				'🐜',
				'🐝',
				'🍉',
				'🍄',
				'🍔',
				'🍤',
				'🍨',
				'🍪',
				'🎂',
				'🍰',
				'🍾',
				'🍷',
				'🍸',
				'🍺',
				'🌍',
				'🚑',
				'⏰',
				'🌙',
				'🌝',
				'🌞',
				'⭐',
				'🌟',
				'🌠',
				'🌨',
				'🌩',
				'⛄',
				'🔥',
				'🎄',
				'🎈',
				'🎉',
				'🎊',
				'🎁',
				'🎗',
				'🏀',
				'🏈',
				'🎲',
				'🔇',
				'🔈',
				'📣',
				'🔔',
				'🎵',
				'🎷',
				'💰',
				'🖊',
				'📅',
				'✅',
				'❎',
				'💯'
			]
		},
		embedded: {
			icon: embedded,
			className: undefined,
			component: undefined,
			popupClassName: undefined,
			defaultSize: {
				height: 'auto',
				width: 'auto'
			}
		},
		image: {
			icon: image,
			className: undefined,
			component: undefined,
			popupClassName: undefined,
			urlEnabled: true,
			uploadEnabled: true,
			alignmentEnabled: true,
			uploadCallback: true,
			previewImage: true,
			inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg',
			alt: { present: true, mandatory: true },
			defaultSize: {
				height: 'auto',
				width: 'auto'
			}
		},
		remove: { icon: remove, className: undefined, component: undefined },
		history: {
			inDropdown: false,
			className: undefined,
			component: undefined,
			dropdownClassName: undefined,
			options: [ 'undo', 'redo' ],
			undo: { icon: undo, className: undefined },
			redo: { icon: redo, className: undefined }
		}
	}
};

class Wysiwyg extends Component {
	constructor(props) {
		super(props);
		const contentState = convertFromRaw(content);

		this.state = {
			contentState
		};
		this.state = {
			editorState: undefined
		};
	}

	onEditorStateChange = (editorState) => {
		this.setState({
			editorState
		});
	};

	onContentStateChange = (contentState) => {
		this.setState({
			contentState
		});
	};

	render() {
		return (
			<main>
				<section className="container wysiwyg-editor">
					<Editor
						wrapperClassName="demo-wrapper"
						editorClassName="demo-editor"
						onContentStateChange={this.onContentStateChange}
						onEditorStateChange={this.onEditorStateChange}
						toolbar={content.toolbarOptions}
					/>{' '}
					<textarea disabled value={JSON.stringify(this.state.contentState, null, 4)} />
					<textarea
						disabled
						value={
							this.state.editorState &&
							draftToMarkdown(convertToRaw(this.state.editorState.getCurrentContent()))
						}
					/>
					<button className="button">Save project</button>
				</section>
			</main>
		);
	}
}

export default Wysiwyg;
