import React, { Component } from 'react';
import illustration from '../assets/images/illustration.svg';
import Snap from 'snapsvg-cjs';
import mina from 'snapsvg-cjs';

class IllustrationLogo extends Component {
	componentDidMount() {
		var snapLogo = Snap('#illustration-logo');
		var firstTimeBlinks = 0;

		this.newMethod(snapLogo, animateIllustration);

		function animateIllustration() {
			var eyeOpen = snapLogo.select('#illustration__eye');
			var eyeClosed = snapLogo.select('#illustration__eye-closed');
			var eyeOpenPoints = eyeOpen.node.getAttribute('d');
			var eyeClosedPoints = eyeClosed.node.getAttribute('d');

			var closeEye = function() {
				var randomTime = Math.floor(Math.random() * (7 - 4 + 1)) + 4;
				if (firstTimeBlinks === 0) {
					firstTimeBlinks++;
					setTimeout(function() {
						eyeOpen.animate({ d: eyeClosedPoints }, 100, mina.easeinout, openEye);
					}, 600);
				} else if (firstTimeBlinks < 2) {
					firstTimeBlinks++;
					setTimeout(function() {
						eyeOpen.animate({ d: eyeClosedPoints }, 100, mina.easeinout, openEye);
					}, 100);
				} else {
					setTimeout(function() {
						eyeOpen.animate({ d: eyeClosedPoints }, 100, mina.easeinout, openEye);
					}, randomTime * 1000);
				}
			};
			var openEye = function() {
				eyeOpen.animate({ d: eyeOpenPoints }, 100, mina.easeinout, closeEye);
			};

			closeEye();
		}
	}

	newMethod(snapLogo, animateIllustration) {
		Snap.load(illustration, function(fragment) {
			snapLogo.append(fragment);
			animateIllustration();
		});
	}

	render() {
		return <div id="illustration-logo" className="illustration__wrapper" />;
	}
}

export default IllustrationLogo;
