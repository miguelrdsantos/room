import React, { Component } from 'react';
import Logo from './Logo';

class Header extends Component {
	render() {
		return (
			<header className="header">
				<div className="header-content">
					<Logo />{' '}
				</div>
			</header>
		);
	}
}

export default Header;
