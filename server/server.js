/* server.js */
// … Other required modules
const mongoose = require('mongoose');

// … Koa code
mongoose.connect('mongodb: //<dbuser>:<dbpassword>@eg12345.mlab.com:12345/my_database')
module.exports = app