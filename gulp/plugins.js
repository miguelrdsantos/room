// ******************************************************************* //
// ************************** Gulp modules *************************** //
// ******************************************************************* //

var gulp        = require('gulp'),
    watch       = require('gulp-watch'),                   // Creates a watcher that will spy on files
    rename      = require('gulp-rename'),                  // Rename files
    notify      = require('gulp-notify'),                  // Send messages/notifications
    plumber     = require('gulp-plumber'),                 // Prevent pipe breaking caused by errors
    svgsprite   = require('gulp-svg-sprite'),              // SVG sprites and optimizations
	svgo	    = require('gulp-svgo'),                    // Optimizing SVG vector graphics files with Gulp
	cheerio     = require('gulp-cheerio');                 // Manipulate HTML and XML files
