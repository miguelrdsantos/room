module.exports = {
    svg: {
        entry: './src/svg/',
        all: './src/svg/**/**/*.svg',
        dest: './src/assets/images/',

        sprite: {
            entry: './src/svg/sprite/',
            all: './src/svg/sprite/**/*.svg'
        },

        icons: {
            entry: './src/svg/icons/',
            all: './src/svg/icons/**/*.svg'
        }
    },

    styles: {
        entry: './src/styles/',
        all: './src/styles/**/*.scss',
        main: './src/styles/main.scss',
        dest: './src/'
    }
}