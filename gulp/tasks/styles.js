// ******************************************************************* //
// ************************** SVG files **************************** //
// ******************************************************************* //

var paths = require('../paths'),
    gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();


// == SVG - Generic ==================================
gulp.task('styles', function () {
    return gulp.src([paths.styles.main])
        .pipe(plugins.sass({
            outputStyle: 'compressed'
        }).on('error', plugins.sass.logError))
        .pipe(gulp.dest(paths.styles.dest));
});