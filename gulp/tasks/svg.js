// ******************************************************************* //
// ************************** SVG files **************************** //
// ******************************************************************* //

var paths = require('../paths'),
    gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

// ************************** SVG Sprite config **************************** //
var svgConfig = {
    shape: {
        id: { // SVG shape ID related options
            whitespace: '-' // Whitespace replacement for shape IDs
        },
        spacing: { // Add padding
            padding: 0
        }
    },
    svg: {
        transform: ['svgo'] // List of transformations / optimizations
    },
    mode: {
        symbol: { // symbol mode to build the SVG
            render: {
                css: false, // CSS output option for icon sizing
                scss: false // SCSS output option for icon sizing
            },
            inline: false,
            dimensions: "-dims",
            dest: './', // destination folder
            prefix: '.icons--%s', // BEM-style prefix if styles rendered
            sprite: 'sprite.svg', // Generated sprite name (temp)
            example: true // Build a sample page, please!
        }
    }
};



// == SVG - Generic ==================================
gulp.task('svg-sprite-images', function () {
    return plugins.watch([paths.svg.sprite.all], function () {

        var spriteDirFile = "sprite";

        gulp.src([paths.svg.entry + '/' + spriteDirFile + '/**/*.svg'])
            .pipe(plugins.plumber())

            .pipe(plugins.svgSprite(svgConfig))
            .on('error', function (error) {})

            .pipe(plugins.rename(function (path) {
                path.basename = spriteDirFile;
            }))

            .pipe(plugins.cheerio({
                run: function ($, file) {
                    var $svg = $('svg');
                    //$svg.addClass(spriteClass);
                    $svg.attr('aria-labelledby', 'title');
                    $svg.attr('role', 'img');

                    $('symbol').each(function () {
                        var symbolTitle = $(this).attr('id').replace('-', ' '),
                            viewboxVal = $(this).attr('viewbox');

                        symbolTitle = symbolTitle.substring(0, 1).toUpperCase() + symbolTitle.substring(1);

                        //If is in item folder
                        $(this).attr('preserveAspectRatio', 'xMidYMid meet');


                        //If is logo remove main color (to be customizable on CSS)
                        if ($(this).attr('id').indexOf('logo--') >= 0) {
                            $(this).find("[fill='#FFF']").removeAttr('fill');
                        }

                        //Fix viewbox issue
                        $(this).removeAttr('viewbox');
                        $(this).attr('viewBox', viewboxVal);

                        if ($(this).children('title').length) {
                            $(this).children('title').text(symbolTitle);
                        } else {
                            $(this).prepend("<title>" + symbolTitle + "</title>");
                        }
                    });
                }
            }))

            .pipe(gulp.dest(paths.svg.dest))

            .pipe(plugins.notify({
                title: "Gulp SVG",
                message: "Amazing " + spriteDirFile + "! <%= file.relative %>"
            }))
    });
});


// == SVG - Icons ==================================
gulp.task('svg-sprite-icons', function () {
    return plugins.watch([paths.svg.icons.all], function () {

        var spriteDirFile = "icons";

        gulp.src(paths.svg.entry + spriteDirFile + '/**/*.svg')
            .pipe(plugins.plumber())

            .pipe(plugins.svgSprite(svgConfig))

            .pipe(plugins.rename(function (path) {
                path.basename = spriteDirFile;
            }))

            .pipe(plugins.cheerio({
                run: function ($, file) {
                    var $svg = $('svg');
                    //$svg.addClass(spriteClass);
                    $svg.attr('aria-labelledby', 'title');
                    $svg.attr('role', 'img');
                    $svg.find('[fill]').removeAttr('fill');

                    $('symbol').each(function () {
                        var symbolTitle = $(this).attr('id').replace('-', ' ');
                        viewboxVal = $(this).attr('viewbox');

                        symbolTitle = symbolTitle.substring(0, 1).toUpperCase() + symbolTitle.substring(1);

                        $(this).attr('preserveAspectRatio', 'xMidYMid');

                        $(this).removeAttr('viewbox');
                        $(this).attr('viewBox', viewboxVal);

                        if ($(this).children('title').length) {
                            $(this).children('title').text(symbolTitle);
                        } else {
                            $(this).prepend("<title>" + symbolTitle + "</title>");
                        }
                    });
                }
            }))
            .on('error', function (error) {})

            .pipe(gulp.dest(paths.svg.dest))

            .pipe(plugins.notify({
                title: "Gulp SVG",
                message: "Amazing " + spriteDirFile + "! <%= file.relative %>"
            }))
    });
});