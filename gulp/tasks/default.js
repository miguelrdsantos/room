var paths = require('../paths'),
    gulp = require('gulp'),
    plugins = require('gulp-load-plugins')();

//task para o watch
gulp.task('watch', function () {
    gulp.watch(paths.styles.all, ['styles']);
});

gulp.task('default', ['styles', 'watch'], function () {
    gulp.start('svg-sprite-icons');
    gulp.start('svg-sprite-images');
});